package com.gkemayo.contact.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gkemayo.contact.service.ContactService;
import com.gkemayo.contact.service.dto.ContactDto;
import com.gkemayo.contact.service.exception.AnnuaryDataAccessException;
import com.gkemayo.contact.service.exception.ContactNotFoundException;

@ExtendWith(SpringExtension.class)
public class ContactRestControllerTest {

	@InjectMocks
	private ContactRestController contactRestController;

	@Mock
	private ContactService contactService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testFindContactByEmail_with_content_ok() throws Exception {
		ContactDto contactDto = ContactDto.builder().id("6316868375").firstName("Leonardo").lastName("Amor")
				.email("lamor0@utexas.edu").company("Devshare").departement("Sales").build();
		
		when(contactService.findContactByEmail(Mockito.anyString())).thenReturn(contactDto);

		ResponseEntity<ContactDto> response = contactRestController.findContactByEmail("lamor0@utexas.edu");

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(response.getBody());
		assertEquals(contactDto.getId(), response.getBody().getId());
		assertEquals(contactDto.getEmail(), response.getBody().getEmail());

	}

	@Test
	public void testFindContactByEmail_NoContactFoundException() throws Exception {

		when(contactService.findContactByEmail(Mockito.anyString()))
				.thenThrow(new ContactNotFoundException("ContactNotFound", "Contact not found", HttpStatus.NO_CONTENT));

		ResponseEntity<ContactDto> response = contactRestController.findContactByEmail("lamor0@utexas.edu");

		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

	@Test
	public void testFindContactByEmail_AnnuaryDataAccessException() throws Exception {
		
		when(contactService.findContactByEmail(Mockito.anyString()))
				.thenThrow(new AnnuaryDataAccessException("DataBaseAccesError", "Database inacessible", HttpStatus.SERVICE_UNAVAILABLE));

		ResponseEntity<ContactDto> response = contactRestController.findContactByEmail("lamor0@utexas.edu");

		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
	}

	// De même pour les tests de la méthode findByLastName
}
