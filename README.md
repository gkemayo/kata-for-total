# Annuaire de recherche

[[_TOC_]]

Mode d'emploi pour un déploiement à la mano :

### 0. Prerequis

Il faut avoir les stack Angular-cli/Nodejs et Maven/JVM d'installer sur la machine d'exécution

### 1. Le Front-end (développé avec Angular en version 7)

 est accessible sur lien http://localhost:4200/annuaire-ui#/

 Pour ce faire, une fois que vous avez cloné le projet :

  - Se placer à la racine du dossier annuaire-ui, ouvrir un Terminal et exécuter la commande suivante pour installer les paquets node_modules :    
            npm update 

  - Pour compiler l'application front, se placer à la racine du dossier annuaire-ui, ouvrir un Terminal et compiler à l'aide de la commande :  
            ng build --prod --base-href /annuaire-ui

  - Pour démarrer l'application front, se placer dans le dossier /annuaire-ui/dist/annuaire-ui, ouvrir un Terminal et exécuter la commande :  
            npm start

### 2. Le Back-end (développé en Java8-maven-SpringBoot2.4)

 est accessible sur le lien http://localhost:8081/annuaire/

 Pour ce faire, une fois que vous avez cloné le projet :

  - L'emplacement du fichier de données initiales (mock_data.csv) a été paramétré dans l'application.properties (champ : path.to.file). Changer la  valeur de ce champ pour le faire pointer à l'endroit où se trouve mock-data.csv sur votre machine.

  - Pour compiler l'application back, se placer à la racine du dossier annuaire, ouvrir un Terminal et compiler à l'aide de la commande :
            mvn package

  - Pour démarrer l'application back, se placer dans le dossier /annuaire/target, ouvrir un Terminal et exécuter la commande :
                            java -jar annuaire-0.0.1-SNAPSHOT.jar 
            
### 3. Le fichier de log de l'application back

 est généré dans le dossier /annuaire/logs

### 4. La base de données embarquée H2

est accessible sur l'url suivante quand l'application back est démarrée : http://localhost:8081/annuaire/h2-console/

| **jdbc url** | **Login** | **password** |
|:----------:|:-----------:|:-----------:|
|    jdbc:h2:file:./src/main/resources/database/annuaire-db    |     sa   |     ""    |

NB : Au cas où vous aurez du mal à réaliser l'étape de compilation du projet front pour cause d'incompatibilité de la version Angular (si la version Angular de la machine exécutante est inférieure à la version 7), j'ai commité de façon exceptionnelle le dossier /dist.

NB : Une livraison dans le cloud ou via une image Docker simplifierai le process ci-dessus... :)

| **Front End** | **Back End** | **Console BDD** |
|:----------:|:-----------:|:-----------:|
|    http://localhost:4200/annuaire-ui#/    |     http://localhost:8081/annuaire/   |     http://localhost:8081/annuaire/h2-console/    |


Enjoy !!!
